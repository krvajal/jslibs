/* ***** BEGIN LICENSE BLOCK *****
 * Version: GNU GPL 2.0
 *
 * The contents of this file are subject to the
 * GNU General Public License Version 2.0; you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * ***** END LICENSE BLOCK ***** */


#pragma once

#include <jlhelper.h>
#include <jlclass.h>
//#include <jsvalserializer.h>
#include <../jslang/handlePub.h>
#include <../jslang/blobPub.h>

//#include <jsdate.h>

#include <queue.h>
#include <stack.h>

// WIN32_LEAN_AND_MEAN removes the following headers files from windows.h:
#include <shellapi.h>
#include <wincrypt.h>
#include <commdlg.h>

#include <shlobj.h> // CSIDL_*, SHGetFolderPath, TB_BUTTONCOUNT, ...
