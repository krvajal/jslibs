Project home: http://oglft.sourceforge.net/
License: GNU Library or Lesser General Public License (LGPL)
Files downloaded from: http://oglft.svn.sourceforge.net/viewvc/oglft/trunk/
Modifications:
	OGLFT.cpp:
	- comment out the Library class definition.
	OGLFT.h:
	- comment out #cmakedefine directives
	- #define OGLFT_API as empty
	- add CALLBACK behind vertexCallback, beginCallback, endCallback, combineCallback and errorCallback declarations (Filled class)
