/* ***** BEGIN LICENSE BLOCK *****
 * Version: GNU GPL 2.0
 *
 * The contents of this file are subject to the
 * GNU General Public License Version 2.0; you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * ***** END LICENSE BLOCK ***** */

#include "stdafx.h"
#include <rsvg-private.h>
#include <rsvg-structure.h>

//#include <libxml/xpath.h>
//#include <libxml/parser.h>

#include <libxml/xmlerror.h>

DECLARE_CLASS( SVG )

struct CxObj {

	JSContext *cx;
	JSObject *obj;
};


bool RequestPixbufImage(JSContext *cx, JSObject *obj, const char *name, GdkPixbuf **pixbuf) {

	*pixbuf = NULL;
	jsval onImageFct;
	JL_CHK( JS_GetProperty(cx, obj, "onImage", &onImageFct) );
	if ( JL_ValueIsCallable(cx, onImageFct) ) {

		jsval nameVal, image;
		JL_CHK( jl::setValue(cx, &nameVal, name) );
		JL_CHK( JS_CallFunctionValue(cx, obj, onImageFct, 1, &nameVal, &image) );

		if ( JSVAL_IS_OBJECT( image ) ) {

			int sWidth, sHeight, sChannels;
			ImageDataType dataType;
			JLData data = JL_GetImageObject(cx, image, &sWidth, &sHeight, &sChannels, &dataType);
			JL_ASSERT( data.IsSet(), E_STR("image"), E_ARG, E_INVALID );
			JL_ASSERT( dataType == TYPE_UINT8, E_DATATYPE, E_INVALID );
			JL_ASSERT( sChannels == 3 || sChannels == 4, E_PROP, E_NAME("channels"), E_RANGE, E_INTERVAL_NUM(3, 4), E_COMMENT(name) ); // "Unsupported image format for %s.", name

			*pixbuf = gdk_pixbuf_new_from_data((const guchar *)data.GetConstStr(), GDK_COLORSPACE_RGB, sChannels == 4, 8, sWidth, sHeight, sWidth*sChannels, NULL, NULL);
			JL_ASSERT( *pixbuf == NULL, E_STR("image"), E_CREATE );
		}
	}
	return true;
	JL_BAD;
}



// see jslibs/libs/librsvg/win32_config/rsvg-image.c at rsvg_pixbuf_new_from_href function.
extern "C" GdkPixbuf *rsvg_pixbuf_new_from_href(const char *href, const char *base_uri, GError ** error) {

	JL_IGNORE(error);

	JSContext *cx = ((CxObj*)base_uri)->cx;
	JSObject *obj = ((CxObj*)base_uri)->obj;
	GdkPixbuf *pixbuf;
	RequestPixbufImage(cx, obj, href, &pixbuf); // error is managed in Write(), after rsvg_handle_write() call.
	return pixbuf;
}

struct Private {
	RsvgHandle *handle;
	cairo_matrix_t transformation;
};



/**doc
$CLASS_HEADER
$SVN_REVISION $Revision$
**/
BEGIN_CLASS( SVG ) // Start the definition of the class. It defines some symbols: _name, _class, _prototype

DEFINE_FINALIZE() { // called when the Garbage Collector is running if there are no remaing references to this object.

	if ( jl::Host::getJLHost(fop->runtime())->canSkipCleanup ) // see HostRuntime::skipCleanup()
		return;

	Private *pv = (Private*)JL_GetPrivate(obj);
	if ( !pv )
		return;

	g_object_unref(pv->handle);
	JL_freeop(fop, pv);
}


/**doc
$TOC_MEMBER $INAME
 $INAME()
  Constructs a new SVG object.
**/
DEFINE_CONSTRUCTOR() {

	Private *pv = NULL;

	JL_ASSERT_ARGC(0);
	JL_ASSERT_CONSTRUCTING();
	JL_DEFINE_CONSTRUCTOR_OBJ;

	pv = (Private*)JS_malloc(cx, sizeof(Private));
	JL_CHK( pv );
	pv->handle = rsvg_handle_new();
	JL_ASSERT( pv->handle != NULL, E_THISOBJ, E_CREATE ); // "Unable to create rsvg handler."
	cairo_matrix_init_identity(&pv->transformation);
	
	JL_SetPrivate(obj, pv);
	return true;

bad:
	if ( pv ) {

		g_object_unref(pv->handle);
		JS_free(cx, pv);
	}
	return false;
}


/**doc
=== Methods ===
**/

/**doc
$TOC_MEMBER $INAME
 $THIS $INAME( xmlString )
  Adds XML data to the current SVG context.
  $H note
   calls to callback function 'onImage' may occur during this call.
  $H example
  {{{
  svg.write(<svg><circle cx="50" cy="50" r="25" fill="red"/></svg>);
  // or
  svg.write('<svg><circle cx="50" cy="50" r="25" fill="red"/></svg>');
  }}}
**/
DEFINE_FUNCTION( write ) {

	JLData data;

		JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

//	const char *data;
//	size_t length;
//	JL_CHK( JL_JsvalToStringAndLength(cx, &JL_ARG(1), &data, &length) );
	JL_CHK( jl::getValue(cx, JL_ARG(1), &data) );

	GError *error = NULL;
	gboolean status;

//	rsvg_handle_new_from_file
//	xmlResetLastError();

//	rsvg_handle_set_base_uri(handle, "123"); // This can only be called before rsvg_handle_write()

	gchar *tmp = handle->priv->base_uri;
	CxObj cxobj;
	cxobj.cx = cx;
	cxobj.obj = obj;
	handle->priv->base_uri = (gchar*)&cxobj; // hack base_uri to store cx and obj for rsvg_pixbuf_new_from_href()
	status = rsvg_handle_write(handle, (const guchar *)data.GetConstStr(), data.Length(), &error);
	handle->priv->base_uri = tmp;

	if ( JL_IsExceptionPending(cx) )
		return false;

	if ( !status ) {

		xmlErrorPtr xmlErr = xmlGetLastError();
		if ( xmlErr != NULL ) // XML error
			JL_ERR( E_ARG, E_NUM(1), E_FORMAT, E_DETAILS, E_STR(xmlErr->message) ); //("SVG error: %s. %s", error->message, xmlErr->message);
		else
			JL_ERR( E_ARG, E_NUM(1), E_INVALID, E_DETAILS, E_STR(error->message) ); // JL_REPORT_ERROR("SVG error: %s", error->message);
	}

	*JL_RVAL = OBJECT_TO_JSVAL(obj);
	return true;
	JL_BAD;
}


/*
DEFINE_PROPERTY( xmlData ) {

	RsvgHandle *handle = (RsvgHandle*)JL_GetPrivate(obj);
	if ( handle )
		g_object_unref(handle);
	handle = rsvg_handle_new();
	JL_ASSERT( handle != NULL, "Unable to create rsvg handler." );
	JL_SetPrivate( obj, handle);

	const char *data;
	size_t length;
	JL_CHK( JL_JsvalToStringAndLength(cx, &JL_ARG(1), &data, &length) );

	GError *error = NULL;
	gboolean status;

//	xmlResetLastError();
//	rsvg_handle_set_base_uri(handle, "123"); // This can only be called before rsvg_handle_write()

	gchar *tmp = handle->priv->base_uri;
	CxObj cxobj = { cx, obj };
	handle->priv->base_uri = (gchar*)&cxobj; // hack base_uri to store cx and obj for rsvg_pixbuf_new_from_href()
	status = rsvg_handle_write(handle, (const guchar *)data, length, &error);
	handle->priv->base_uri = tmp;

	if ( JL_IsExceptionPending(cx) )
		return false;

	if ( !status ) {

		xmlErrorPtr xmlErr = xmlGetLastError();
		if ( xmlErr != NULL ) { // XML error
			JL_REPORT_ERROR("SVG error: %s. %s", error->message, xmlErr->message);
		} else
			JL_REPORT_ERROR("SVG error: %s", error->message);
	}
	return true;
}
*/

/**doc
$TOC_MEMBER $INAME
 $TYPE ImageObject $INAME( [ imageWidth , imageHeight ] [ , channels ] [ , fit ] [ , elementId ] )
  Draws the SVG to an image object.
  $H arguments
   $ARG $INT imageWidth: override default SVG's  width.
   $ARG $INT imageHeight: override default SVG's  width.
   $ARG $INT channels: 1 (Alpha only), 3 (RGB) or 4 (RGBA).
   $ARG $BOOL fit: fit the SVG dimensions to [imageWidth, imageHeight].
   $ARG $STR elementId: draws a subset of a SVG starting from an element's id. For example, if you have a layer called "layer1" that you wish to render, pass "#layer1" as the id.
  $H example
  {{{
  var svg = new SVG();
  svg.write(<svg><circle cx="50" cy="50" r="25" fill="red"/></svg>);
  svg.rotate(Math.PI/4); // +45 deg
  svgimage = svg.renderImage(100, 100, true);
  new File('test.png').content = encodePngImage( svgimage )
  }}}
**/
DEFINE_FUNCTION( renderImage ) { // using cairo

	JLData id;

		JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	gboolean status;

	GError *error = NULL;
	status = rsvg_handle_close(handle, &error);
	JL_CHKM( status, E_LIB, E_STR("rsvg"), E_INTERNAL, E_DETAILS, E_STR(error->message) );

	RsvgDimensionData dim;
	rsvg_handle_get_dimensions(handle, &dim);

	size_t imageWidth, imageHeight;
	if ( JL_ARG_ISDEF(1) ) {

		JL_ASSERT_ARGC_MIN(2);
		JL_CHK( jl::getValue(cx, JL_ARG(1), &imageWidth) );
		JL_CHK( jl::getValue(cx, JL_ARG(2), &imageHeight) );
	} else {

		imageWidth = dim.width;
		imageHeight = dim.height;
	}

	size_t channels;
	if ( JL_ARG_ISDEF(3) ) {

		JL_CHK( jl::getValue(cx, JL_ARG(3), &channels) );
		JL_ASSERT( channels == 1 || channels == 3 || channels == 4, E_ARG, E_NUM(3), E_EQUALS, E_NUM(1), E_OR, E_NUM(3), E_OR, E_NUM(4) );
	} else {

		channels = 4;
	}

	if ( JL_ARG_ISDEF(4) ) { // fit

		bool fit;
		JL_CHK( jl::getValue(cx, JL_ARG(4), &fit) );
		if ( fit ) {

			cairo_matrix_t tmp;
			cairo_matrix_init_identity(&tmp);
			cairo_matrix_scale(&tmp, (double)imageWidth / (double)dim.width, (double)imageHeight / (double)dim.height);
			cairo_matrix_multiply(&pv->transformation, &pv->transformation, &tmp);
//			cairo_matrix_scale(&pv->transformation, (double)imageWidth / (double)dim.width, (double)imageHeight / (double)dim.height);
		}
	}

/*
	if ( JL_ARG_ISDEF(4) ) {

		JL_ASSERT_ARG_IS_ARRAY(4);
		double trVector[6];
		size_t currentLength;
		JL_CHK( jl::getVector(cx, JL_ARG(4), trVector, COUNTOF(trVector), &currentLength ) );
		JL_ASSERT( currentLength == 6, "Invalid transformation matrix size." );
		cairo_matrix_t tmp = *(cairo_matrix_t*)&trVector;
		cairo_matrix_multiply(&tr, &tmp, &tr);
	}
*/

//	const char *id;
	if ( JL_ARG_ISDEF(5) ) {

		JL_CHK( jl::getValue(cx, JL_ARG(5), &id) );
		JL_ASSERT( id.IsSet() && id.GetConstStrZ()[0] == '#', E_ARG, E_NUM(5), E_INVALID );
	}

	cairo_format_t surfaceFormat;
	switch ( channels ) {
		case 1:
			surfaceFormat = CAIRO_FORMAT_A8;
			break;
		case 3:
			surfaceFormat = CAIRO_FORMAT_RGB24;
			break;
		case 4:
			surfaceFormat = CAIRO_FORMAT_ARGB32; // Pre-multiplied alpha is used. (That is, 50% transparent red is 0x80800000, not 0x80ff0000.)
			break;
		default:
			JL_ERR( E_PARAM, E_STR("channels"), E_EQUALS, E_NUM(1), E_OR, E_NUM(3), E_OR, E_NUM(4) ); // "unsupported output image format"
	}

	cairo_surface_t *surface = cairo_image_surface_create(surfaceFormat, imageWidth, imageHeight);
	//JL_ASSERT( cairo_surface_status(surface) == CAIRO_STATUS_SUCCESS, "Unable to create a drawing surface." );
	JL_ASSERT( cairo_surface_status(surface) == CAIRO_STATUS_SUCCESS, E_LIB, E_STR("cairo"), E_INTERNAL );

	cairo_t *cr = cairo_create(surface);
	//JL_ASSERT( cairo_status(cr) == CAIRO_STATUS_SUCCESS, "Unable to create drawing state." );
	JL_ASSERT( cairo_status(cr) == CAIRO_STATUS_SUCCESS, E_LIB, E_STR("cairo"), E_INTERNAL );

	//cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
	//cairo_set_tolerance(cr, 0.1);

	cairo_set_matrix(cr, &pv->transformation);
	status = rsvg_handle_render_cairo_sub(handle, cr, id.GetStrConstOrNull());

	//JL_ASSERT( status == TRUE, "Unable to render the SVG. %s", cairo_status_to_string(cairo_status(cr)) );
	JL_ASSERT( status == TRUE, E_LIB, E_STR("cairo"), E_INTERNAL, E_DETAILS, E_STR(cairo_status_to_string(cairo_status(cr))) );

//	cairo_format_t surfaceFormat = cairo_image_surface_get_format(surface);

	int width = cairo_image_surface_get_width(surface);
	int height = cairo_image_surface_get_height(surface);
	void *buffer = cairo_image_surface_get_data(surface);

	uint8_t *image = JL_NewImageObject(cx, width, height, channels, TYPE_UINT8, JL_RVAL);
	JL_CHK( image );

	size_t pixelCount = width * height;

	switch ( surfaceFormat ) {
		case CAIRO_FORMAT_RGB24:
			for ( size_t i = 0; i < pixelCount; i++ ) { // 3 2 1 X  ->  1 2 3

				uint32_t src = ((uint32_t*)buffer)[i];
				uint8_t* dest = (uint8_t*)image + i*3;
				*(dest++) = src >> 16 & 0xff;
				*(dest++) = src >> 8 & 0xff;
				*(dest++) = src & 0xff;
			}
			break;
		case CAIRO_FORMAT_ARGB32:
			for ( size_t i = 0; i < pixelCount; i++ ) { // 3 2 1 4  ->  1 2 3 4

				uint32_t p = ((uint32_t*)buffer)[i];
				((uint32_t*)image)[i] = p & 0xff00ff00 | _rotl(p, 16) & 0x00ff00ff; // use SSE2 instructions ?
			}
			break;
		case CAIRO_FORMAT_A8:
			jl::memcpy(image, buffer, pixelCount * channels);
			break;
	}

	cairo_destroy(cr);
	cairo_surface_destroy(surface);

	return true;
	JL_BAD;
}


/*
DEFINE_FUNCTION( getImage ) { // using pixbuf

	RsvgHandle *handle = (RsvgHandle*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(handle);

	GdkPixbuf *pb = rsvg_handle_get_pixbuf(handle);

	JL_ASSERT( pb != NULL, "Insufficient data has been read to create the pixbuf." );
	JL_ASSERT( gdk_pixbuf_get_bits_per_sample(pb) == 8, "Unsupported bits_per_sample." );

	int width = gdk_pixbuf_get_width(pb);
	int height = gdk_pixbuf_get_height(pb);
	int channels = gdk_pixbuf_get_n_channels(pb);
	void *buffer = gdk_pixbuf_get_pixels(pb);

	jsval blobVal;
	JL_CHK( JL_NewBlob(cx, buffer, width*height*channels, &blobVal) ); // (TBD) copy the buffer before !!

	JSObject *blobObj;
	JL_CHK( JS_ValueToObject(cx, blobVal, &blobObj) );
	JL_ASSERT( blobObj, "Unable to create Blob object." );
	*JL_RVAL = OBJECT_TO_JSVAL(blobObj);

	JL_CHK( JS_DefineProperty(cx, blobObj, "channels", INT_TO_JSVAL(channels), NULL, NULL, JSPROP_READONLY | JSPROP_PERMANENT ) );
	JL_CHK( JS_DefineProperty(cx, blobObj, "width", INT_TO_JSVAL(width), NULL, NULL, JSPROP_READONLY | JSPROP_PERMANENT ) );
	JL_CHK( JS_DefineProperty(cx, blobObj, "height", INT_TO_JSVAL(height), NULL, NULL, JSPROP_READONLY | JSPROP_PERMANENT ) );

	return true;
}
*/


/**doc
$TOC_MEMBER $INAME
 $BOOL $INAME( elementId, polarity )
  $H arguments
   $ARG $STR elementId: the id of the element with '#' prefix (eg. '#circle1').
   $ARG $BOOL polarity: false to hide, true to show.
  $H return value
   true if the element visibility has been set, otherwise false.
**/
DEFINE_FUNCTION( setVisible ) {

	JLData id;

		JL_ASSERT_THIS_INSTANCE();
	JL_ASSERT_ARGC_MIN(2);

	Private *pv = (Private*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	JL_CHK( jl::getValue(cx, JL_ARG(1), &id) );
	JL_ASSERT( id.IsSet() && id.GetConstStrZ()[0] == '#', E_ARG, E_NUM(1), E_INVALID );

	bool visible;
	JL_CHK( jl::getValue(cx, JL_ARG(2), &visible) );

	RsvgNode *drawsub = rsvg_defs_lookup (handle->priv->defs, id.GetStrConstOrNull());

	if ( drawsub == NULL ) {

		*JL_RVAL = JSVAL_FALSE;
		return true;
	}

	drawsub->state->visible = visible ? TRUE : FALSE;

	*JL_RVAL = JSVAL_TRUE;
	return true;
	JL_BAD;
}


/**doc
$TOC_MEMBER $INAME
 $THIS $INAME( sx, sy )
  Applies scaling by _sx_, _sy_ to the current transformation.
  The effect of the new transformation is to first scale the coordinates by _sx_ and _sy_,
  then apply the original transformation to the coordinates.
**/
DEFINE_FUNCTION( scale ) {

		JL_ASSERT_THIS_INSTANCE();
	JL_ASSERT_ARGC_MIN(2);

	Private *pv = (Private*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	double sx, sy;
	JL_CHK( jl::getValue(cx, JL_ARG(1), &sx) );
	JL_CHK( jl::getValue(cx, JL_ARG(2), &sy) );
	cairo_matrix_scale(&pv->transformation, sx, sy);

	*JL_RVAL = OBJECT_TO_JSVAL(obj);
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $THIS $INAME( radians )
  Applies rotation by _radians_ to the current transformation.
  The effect of the new transformation is to first rotate the coordinates by _radians_,
  then apply the original transformation to the coordinates.
**/
DEFINE_FUNCTION( rotate ) {

		JL_ASSERT_THIS_INSTANCE();
	JL_ASSERT_ARGC_MIN(1);

	Private *pv = (Private*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	double angle;
	JL_CHK( jl::getValue(cx, JL_ARG(1), &angle) );
	cairo_matrix_rotate(&pv->transformation, angle);

	*JL_RVAL = OBJECT_TO_JSVAL(obj);
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $THIS $INAME( tx, ty )
  Applies a translation by _tx_, _ty_ to the current transformation.
  The effect of the new transformation is to first translate the coordinates by _tx_ and _ty_,
  then apply the original transformation to the coordinates.
**/
DEFINE_FUNCTION( translate ) {

		JL_ASSERT_THIS_INSTANCE();
	JL_ASSERT_ARGC_MIN(2);

	Private *pv = (Private*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	double tx, ty;
	JL_CHK( jl::getValue(cx, JL_ARG(1), &tx) );
	JL_CHK( jl::getValue(cx, JL_ARG(2), &ty) );
	cairo_matrix_translate(&pv->transformation, tx, ty);

	*JL_RVAL = OBJECT_TO_JSVAL(obj);
	return true;
	JL_BAD;
}


/**doc
=== Properties ===
**/

/**doc
$TOC_MEMBER $INAME
 $INT | $ARRAY $INAME $WRITEONLY
  Sets the dpi of the resulting image. If the argument is an Array (like [ dpiX, dpiY ]) X and Y dpi can be set aside.
**/
DEFINE_PROPERTY_SETTER( dpi ) {

	JL_IGNORE(strict, id);

	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	if ( vp.isUndefined() ) {

		rsvg_handle_set_dpi(handle, -1);
	} else
	if ( vp.isNumber() ) {

		size_t dpi;
		JL_CHK( jl::getValue(cx, *vp, &dpi) );
		rsvg_handle_set_dpi(handle, dpi);
	} else
	if ( JL_ValueIsArray(cx, *vp) ) { // array-like does not make sense here.

		size_t dpiX, dpiY;
		jsval tmp;
		JL_CHK( JL_GetElement(cx, &vp.toObject(), 0, &tmp) );
		JL_CHK( jl::getValue(cx, tmp, &dpiX) );
		JL_CHK( JL_GetElement(cx, &vp.toObject(), 1, &tmp) );
		JL_CHK( jl::getValue(cx, tmp, &dpiY) );
		rsvg_handle_set_dpi_x_y(handle, dpiX, dpiY);
	}
	
	JL_ERR( E_VALUE, E_TYPE, E_TY_UNDEFINED, E_OR, E_TY_UNDEFINED, E_OR, E_TY_ARRAY );
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
  Is the default width of the SVG.
**/
DEFINE_PROPERTY_GETTER( width ) {

	JL_IGNORE(id);
	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	RsvgDimensionData dim;
	rsvg_handle_get_dimensions(handle, &dim);
	JL_CHK( jl::setValue(cx, vp, dim.width) );
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
  Is the default height of the SVG.
**/
DEFINE_PROPERTY_GETTER( height ) {

	JL_IGNORE(id);
	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	RsvgDimensionData dim;
	rsvg_handle_get_dimensions(handle, &dim);
	JL_CHK( jl::setValue(cx, vp, dim.height) );
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
  Is the title of the SVG.
**/
DEFINE_PROPERTY_GETTER( title ) {

	JL_IGNORE(id);
	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	const char *title = rsvg_handle_get_title(handle);
	if ( title != NULL )
		JL_CHK( jl::setValue(cx, vp, title) );
	else
		*vp = JSVAL_VOID;
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
  Is the metadata string of the SVG.
**/
DEFINE_PROPERTY_GETTER( metadata ) {

	JL_IGNORE(id);
	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	const char *metadata = rsvg_handle_get_metadata(handle);
	if ( metadata != NULL )
		JL_CHK( jl::setValue(cx, vp, metadata) );
	else
		*vp = JSVAL_VOID;
	return true;
	JL_BAD;
}

/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
  Is the description string of the SVG.
**/
DEFINE_PROPERTY_GETTER( description ) {

	JL_IGNORE(id);
	JL_ASSERT_THIS_INSTANCE();

	Private *pv = (Private*)JL_GetPrivate(obj);
	JL_ASSERT_THIS_OBJECT_STATE(pv);
	RsvgHandle *handle = pv->handle;

	const char *description = rsvg_handle_get_desc(handle);
	if ( description != NULL )
		JL_CHK( jl::setValue(cx, vp, description) );
	else
		*vp = JSVAL_VOID;
	return true;
	JL_BAD;
}


/**doc
$TOC_MEMBER $INAME
 $INAME $READONLY
**/
/*
DEFINE_PROPERTY(images) {

	RsvgHandle *handle = (RsvgHandle*)JL_GetPrivate(JL_OBJ);
	JL_ASSERT_THIS_OBJECT_STATE(handle);

	JL_CHK( JL_GetReservedSlot( JL_OBJ, SLOT_IMAGES_OBJECT, vp) );
	if ( JSVAL_IS_VOID( *vp ) ) {

		*vp = OBJECT_TO_JSVAL( jl::newObject(cx) );
		JL_CHK( JL_SetReservedSlot( JL_OBJ, SLOT_IMAGES_OBJECT, *vp) );
	}
	return true;
}
*/


/**doc
=== callback functions ===
 * $TYPE ImageObject *onImage*( uri )
  Called when the SVG renderer need to draw an image element. _uri_ is the name of the requested image. The function must return an image Object.
  $H example
  {{{
  var svg = new SVG();
  svg.write(<svg><image x="0" y="0" path="img.png"/></svg>);
  svg.onImage = function(href) {
   
   return decodePngImage( new File('myImage.png').open('r') );
  }
  }}}
**/

CONFIGURE_CLASS

	REVISION(jl::SvnRevToInt("$Revision$"))
	HAS_PRIVATE

	HAS_CONSTRUCTOR
	HAS_FINALIZE

	BEGIN_FUNCTION_SPEC
		FUNCTION(write)
		FUNCTION(renderImage)
		FUNCTION(setVisible)
//		FUNCTION(getImage)
		FUNCTION(scale)
		FUNCTION(rotate)
		FUNCTION(translate)
	END_FUNCTION_SPEC

	BEGIN_PROPERTY_SPEC
//		PROPERTY_WRITE_STORE( xmlData )
//		PROPERTY_GETTER( images )
		PROPERTY_SETTER( dpi )
		PROPERTY_GETTER( width )
		PROPERTY_GETTER( height )
		PROPERTY_GETTER( title )
		PROPERTY_GETTER( metadata )
		PROPERTY_GETTER( description )
	END_PROPERTY_SPEC

END_CLASS
