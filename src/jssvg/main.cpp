/* ***** BEGIN LICENSE BLOCK *****
 * Version: GNU GPL 2.0
 *
 * The contents of this file are subject to the
 * GNU General Public License Version 2.0; you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * ***** END LICENSE BLOCK ***** */

#include "stdafx.h"
#include <jslibsModule.cpp>

DECLARE_CLASS( SVG )

/**doc t:header
$MODULE_HEADER
 $FILE_TOC
**/

/**doc t:footer
$MODULE_FOOTER
**/

bool
ModuleInit(JSContext *cx, JSObject *obj, uint32_t id) {

	JL_CHK( InitJslibsModule(cx, id)  );
	rsvg_init();

	struct ReleaseModule : jl::Callback {
		bool operator()( EventType &ev ) {
		
			rsvg_term();
			return true;
		}
	};

	jl::HostRuntime::getJLRuntime(cx).addListener(jl::EventId::AFTER_DESTROY_RUNTIME, new ReleaseModule()); // frees mpv after rt and cx has been destroyed
	

	INIT_CLASS( SVG );
	return true;
	JL_BAD;
}


#ifdef _WINDLL

EXTERN_C BOOL WINAPI pango_DllMain(HINSTANCE, DWORD, LPVOID);

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {

	return pango_DllMain(hinstDLL, fdwReason, lpvReserved);
}

#endif
